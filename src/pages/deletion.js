import React from 'react';

import Layout from '../components/Layout';
import Header from '../components/Header';
import Scroll from '../components/Scroll';
import Footer from '../components/Footer';
import Social from '../components/Social';

import demo1 from '../assets/images/1.jpg';
import demo2 from '../assets/images/2.jpg';

import demo3 from '../assets/images/3.jpg';
import demo4 from '../assets/images/4.jpg';

import gplay from '../assets/images/google-play-badge.svg';
import astore from '../assets/images/app-store-badge.svg';


var imageItem = [demo4, demo1, demo2, demo3]


export default class PrivacyPage extends React.Component {
  constructor(props) {
    super(props);
    this.state ={
      code: ""
    }
  }

  componentDidMount() {
    if(typeof window != undefined ) {
      let queryString = window.location.search;
      let urlParams = new URLSearchParams(queryString);
     let code = urlParams.get('id')
      
      this.setState({
        code:code
      })
    }
  }

  render() {

  

    return (
      <Layout>
<Header />
  Swapee is deleting your Account on facebook with the code: {this.state.code}
<Footer />
</Layout>
    )
  }
}


