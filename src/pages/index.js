import React from 'react';

import Layout from '../components/Layout';
import Header from '../components/Header';
import Scroll from '../components/Scroll';
import Footer from '../components/Footer';
import Social from '../components/Social';

import demo1 from '../assets/images/1.jpg';
import demo2 from '../assets/images/2.jpg';

import demo3 from '../assets/images/3.jpg';
import demo4 from '../assets/images/4.jpg';

import gplay from '../assets/images/google-play-badge.svg';
import astore from '../assets/images/app-store-badge.svg';


var imageItem = [demo4, demo1, demo2, demo3]


export default class IndexPage extends React.Component {
  constructor(props) {
    super(props);
  }

  // componentDidMount() {
  //   var i = 0;  
  //   setInterval(function() {
  //       document.getElementById("header-img").setAttribute("src",imageItem[i]);
  //       if(i == 3){
  //         i = 0;
  //         i++;
  //       }else{
  //       i++;
          
  //       }
  //     }, 3000);
  // }

  render() {
    return (
      <Layout>
<Header />

<header className="masthead">
  <div className="container h-100">
    <div className="row h-100">
      <div className="col-lg-7 my-auto">
        <div className="header-content mx-auto">
          <h1 className="mb-5">
            Use <b>SWAPEE</b> app, as your trading backbone to become a trusted partner!
          </h1>
          <Scroll type="id" element="download">
            <a href="#download" className="btn btn-outline btn">
              Soon for Free!
            </a>
          </Scroll>
        </div>
      </div>
      <div className="col-lg-5 my-auto">
        <div className="device-container">
          <div className="device-mockup iphone6_plus portrait white">
            <div className="device">
              <div className="screen">
                <img id="header-img" src={demo1} className="img-fluid " style={{width:'100%', height:'100%'}} alt="" />
              </div>
              <div className="button"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>

<section className="download bg-primary text-center" id="download">
  <div className="container">
    <div className="row">
      <div className="col-md-8 mx-auto">
        <h3 className="section-heading">
        Our app is available soon on any mobile device!
        </h3>
        <div className="badges">
          <a className="badge-link" href="/#">
            <img src={gplay} alt="" />
          </a>
          <a className="badge-link" href="/#">
            <img src={astore} alt="" />
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

<section className="features" id="features">
  <div className="container">
    <div className="section-heading text-center">
      <h2>WHY YOU'LL LOVE USING <b>SWAPEE</b></h2>
      <p className="text-muted">
        Check out what you can do with this app!
      </p>
      <hr />
    </div>
    <div className="row">
      <div className="col-lg-4 my-auto">
        <div className="device-container">
          <div className="device-mockup iphone6_plus portrait white">
            <div className="device">
              <div className="screen">
              <img src={demo3} className="img-fluid" style={{width:'100%', height:'100%'}} alt="" />
              </div>
              <div className="button"></div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-8 my-auto">
        <div className="container-fluid">
          <div className="row">
          <div className="col-lg-6">
              <div className="feature-item">
                <i className="icon-present text-primary"></i>
                <h3>Look what's trending</h3>
                <p className="text-muted">
                Have fun on searching everything that you desire with <b>SWAPEE</b>. Search for  a swapper’s goods and services with just one tap away. 
                </p>
              </div>
            </div>
            <div className="col-lg-6">
              <div className="feature-item">
                <i className="icon-screen-smartphone text-primary"></i>
                <h3>Find out your and others desire</h3>
                <p className="text-muted">
                  Connect and browse to see what everybody needs and get a bird’s eye-view on all of them.

                </p>
              </div>
            </div>
          </div>
          <div className="row">
          <div className="col-lg-6">
              <div className="feature-item">
                <i className="icon-camera text-primary"></i>
                <h3>Schedule post and reply in real-time</h3>
                <p className="text-muted">
                (Post, Offer and Swap in real-time)<br/>
Post. Start a conversation. Finalize offers and create a plan. Voila! You’ll meet cool peeps along the way.  

                </p>
              </div>
            </div>
            <div className="col-lg-6">
              <div className="feature-item">
                <i className="icon-lock-open text-primary"></i>
                <h3>Save money</h3>
                <p className="text-muted">
                You won’t have to worry, there's no waste of money
                and no any extra expenses!
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section className="cta">
  <div className="cta-content">
    <div className="container">
      <h2>
        Save time.
        <br />
        Show results.
      </h2>
      <Scroll type="id" element="contact">
        <a href="#contact" className="btn btn-outline btn-xl">
          Let's Get Started Soon!
        </a>
      </Scroll>
    </div>
  </div>
  <div className="overlay"></div>
</section>

<section className="contact bg-primary" id="contact">
  <div className="container">
    <h2>
      {/* We
      <i className="fas fa-heart"></i>
      new friends! */}
      Get reach fast, with good leverage!
    </h2>
    <Social />
  </div>
</section>

<Footer />
</Layout>
    )
  }
}


